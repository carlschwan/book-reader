/*
 * Copyright (C) 2020 Emanuele Sorce emanuele.sorce@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Sturm Reader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QIcon>
#include <QLoggingCategory>
#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickStyle>
#include <QString>
#include <QUrl>

#include <libintl.h>
#include <locale>
#include <string>

#include "config-bookreader.h"
#include "filesystem.h"
#include "fontlister.h"
#include "gettext.h"
#include "qhttpserver/fileserver.h"
#include "qhttpserver/qhttprequest.h"
#include "qhttpserver/qhttpresponse.h"
#include "qhttpserver/qhttpserver.h"
#include "reader/cbzreader.h"
#include "reader/epubreader.h"
#include "reader/pdfreader.h"
#include "units.h"

// =================
// Launcher function
// =================
int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    KLocalizedString::setApplicationDomain("bookreader");

    KAboutData aboutData(QStringLiteral("bookreader"), i18nc("@title", "BooKreader"), QStringLiteral(BOOKREADER_VERSION_STRING), i18nc("@title", "A e-book reader"), KAboutLicense::GPL_V3);

    aboutData.addAuthor(i18nc("@info:credit", "Carl Schwan"), i18nc("@info:credit", "Maintainer"), QStringLiteral("carl@carlschwan.eu"), QStringLiteral("https://carlschwan.eu"));
    aboutData.addAuthor(i18nc("@info:credit", "Emanuele Sorce"), i18nc("@info:credit", "Original creator of Ubuntu Touch version"), QStringLiteral("emanuele.sorce@hotmail.com"));

    KAboutData::setApplicationData(aboutData);
    QApplication::setWindowIcon(QIcon::fromTheme(QStringLiteral("org.kde.kbookreader")));

    QCommandLineParser parser;
    aboutData.setupCommandLine(&parser);
    parser.process(app);
    aboutData.processCommandLine(&parser);

    // localization
    Gettext gt;
    // device indipendent pixels
    Units un;
    // Qt available fonts
    FontLister fl;
    // file operations
    FileSystem fs;
    // http server
    QHttpServer http_server;
    FileServer file_server;
    // book parsers
    EpubReader epub;
    PDFReader pdf;
    CBZReader cbz;

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.rootContext()->setContextProperty("gettext", &gt);
    engine.rootContext()->setContextProperty("portable_units", &un);
    engine.rootContext()->setContextProperty("qtfontlist", &fl);
    engine.rootContext()->setContextProperty("filesystem", &fs);
    engine.rootContext()->setContextProperty("fileserver", &file_server);
    engine.rootContext()->setContextProperty("httpserver", &http_server);
    qmlRegisterUncreatableType<QHttpRequest>("HttpUtils", 1, 0, "HttpRequest", "Do not create HttpRequest directly");
    qmlRegisterUncreatableType<QHttpResponse>("HttpUtils", 1, 0, "HttpResponse", "Do not create HttpResponse directly");
    engine.rootContext()->setContextProperty("epubreader", &epub);
    engine.rootContext()->setContextProperty("pdfreader", &pdf);
    engine.rootContext()->setContextProperty("cbzreader", &cbz);

    qmlRegisterType(QUrl(QStringLiteral("qrc:/ImporterPortable.qml")), "Importer", 1, 0, "Importer");
    qmlRegisterType(QUrl(QStringLiteral("qrc:/MetricsPortable.qml")), "Metrics", 1, 0, "Metrics");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
