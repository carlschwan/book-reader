/* Copyright 2020 Emanuele Sorce
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */
import QtQuick.Controls 2.2
import QtQuick 2.9
import QtQuick.Layouts 1.3
import org.kde.kirigami 2.14 as Kirigami

Kirigami.Page {
    id: settings
    property string homepath: filesystem.homePath() + "/"

    header: ToolBar {
        id: aboutheader
        width: parent.width
        RowLayout {
            spacing: scaling.dp(10)
            anchors.fill: parent

            ToolButton {
                padding: scaling.dp(7)
                contentItem: Icon {
                    anchors.centerIn: parent
                    name: "go-previous"
                    color: colors.item
                }
                onClicked: pageStack.pop()
            }

            Label {
                text: i18n("Settings")
                font.pointSize: 22
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignLeft
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }

    Flickable {
        id: flickable
        anchors.fill: parent
        contentHeight:  settingsColumn.height + scaling.dp(80)
        contentWidth: parent.width
        ScrollBar.vertical: ScrollBar { }

        Column {
            id: settingsColumn
            width: parent.width

            // APPARMOR
            ItemDelegate {
                visible: true //!localBooks.readablehome
                width: parent.width
                contentItem: Column {
                    width: parent.width
                    spacing: scaling.dp(5)
                    Label {
                        width: parent.width
                        text: i18n("Default Book Location")
                    }
                    Label {
                        width: parent.width
                        text: i18n("Sturm Reader seems to be operating under AppArmor restrictions that prevent it " +
                            "from accessing most of your home directory.  Ebooks should be put in " +
                            "<i>%1</i> for Sturm Reader to read them.").arg(localBooks.bookdir)
                        wrapMode: Text.Wrap
                    }
                    Button {
                        width: parent.width * 0.95
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: i18n("Reload Directory")
                        // We don't bother with the Timer trick here since we don't get this dialog on
                        // first launch, so we shouldn't have too many books added to the library when
                        // this button is clicked.
                        onClicked: localBooks.readBookDir()
                    }
                }
            }

            // NOT APPARMOR (Not stable yet, more testing is needed before enabling it)
            ItemDelegate {
                visible: false//localBooks.readablehome
                width: parent.width

                contentItem: Column {
                    width: parent.width
                    spacing: scaling.dp(5)
                    Label {
                        width: parent.width
                        text: i18n("Default Book Location")
                    }
                    Label {
                        width: parent.width
                        text: i18n("Enter the folder in your home directory where your ebooks are or " +
                                    "should be stored. Changing this value will not affect existing " +
                                    "books in your library.")
                        wrapMode: Text.Wrap
                    }
                    TextField {
                        id: pathfield
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: parent.width * 0.95
                        text: localBooks.bookdir
                        onTextChanged: {
                            var status = filesystem.exists(pathfield.text);
                            if (status == 0) {
                                /*/ Create a new directory from path given. /*/
                                useButton.text = i18n("Create Directory");
                                useButton.enabled = true;
                            } else if (status == 1) {
                                /*/ File exists with path given. /*/
                                useButton.text = i18n("File Exists");
                                useButton.enabled = false;
                            } else if (status == 2) {
                                if (pathfield.text == localBooks.bookdir && !localBooks.firststart)
                                    /*/ Read the books in the given directory again. /*/
                                    useButton.text = i18n("Reload Directory")
                                else
                                    /*/ Use directory specified to store books. /*/
                                    useButton.text = i18n("Use Directory")
                                useButton.enabled = true;
                            }
                        }
                    }
                    Button {
                        id: useButton
                        width: parent.width * 0.95
                        anchors.horizontalCenter: parent.horizontalCenter
                        onClicked: {
                            var status = filesystem.exists(pathfield.text)
                            if (status != 1) { // Should always be true
                                if (status == 0)
                                    filesystem.makeDir(pathfield.text)
                                localBooks.setBookDir(pathfield.text)
                                useButton.enabled = false
                                unblocker.start()
                            }
                        }
                    }

                    Timer {
                        id: unblocker
                        interval: 10
                        onTriggered: {
                            localBooks.readBookDir()
                            localBooks.firststart = false
                        }
                    }
                }
            }

            SwitchDelegate {
                width: parent.width
                text: i18n("Use legacy PDF viewer")

                onClicked: appsettings.legacypdf = checked
                Component.onCompleted: checked = appsettings.legacypdf
            }
        }
    }
}

